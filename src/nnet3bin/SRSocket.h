#pragma once
#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <iostream>
#include "SRHelper.h"
#include <fstream>
#include <string.h>
#include <time.h>
#ifdef _WIN32
#pragma comment(lib,"ws2_32.lib")
#else
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<string>
#endif

enum MsgType {
	SR_CONTENT,
	SR_QUIT,
	SR_UNKOWN = 0xff
};

#define SIZE_4K  4096
struct SocketHeader {
	int type_	= SR_UNKOWN;
	int length_ = 0;
};
const int header_size = sizeof(SocketHeader);

struct MsgBuffer {
	char* data_ = nullptr;
	int length_ = 0;
};
std::string s2="test";
void DealRecvSpeech(std::string filename);
void SpeechRecg(int cnt,std::string &result);

class SRSocketClient {
public:
	SRSocketClient(int connect_id);
	~SRSocketClient();

	void run();

	bool is_ok();

	static void socket_thread_proc(void* self_ptr);

	static void sr_thread_proc(void* self_ptr);
private:
	void wav_proc(MsgBuffer buffer);
private:
	int		con_id_ = -1;
	bool	running_ = true;	
	std::thread thread_;
	std::thread sr_thread_;
	std::mutex	mutex_;

	std::queue<MsgBuffer>   content_list;
};

typedef std::vector<SRSocketClient*> SocketClients;


class SRSocketSever
{
public:
	explicit  SRSocketSever(int cnt,int port);
	~SRSocketSever();

	void start();

private:
	int		client_count_ = 5;
	SocketClients	clients_;
	int		port_;
};


typedef std::vector<int> IntArray;

MsgBuffer make_buffer(int lenth)
{
	MsgBuffer buffer;
	buffer.data_ = new char[lenth];
	memset(buffer.data_,0, lenth);
	buffer.length_ = lenth;
	return buffer;
}


SRSocketClient::SRSocketClient(int connect_id)
	:con_id_(connect_id)
{

}

SRSocketClient::~SRSocketClient()
{
	if (thread_.joinable())
	{
		thread_.join();
	}
	if (sr_thread_.joinable())
	{
		sr_thread_.join();
	}
}

void SRSocketClient::run()
{
	running_ = true;
	thread_ = std::thread(socket_thread_proc, this);
	sr_thread_ = std::thread(sr_thread_proc, this);
}

void SRSocketClient::socket_thread_proc(void* self_ptr)
{   
    KALDI_LOG<< "开始线程socket_thread_proc！！！";
	SRSocketClient* self = (SRSocketClient*)self_ptr;
	while (self->running_)
	{
        //KALDI_LOG << "thread start read message!"; 
		char buf[header_size];
		int read_count = recv(self->con_id_, buf, header_size, MSG_WAITALL);
		//KALDI_LOG << "thread read count: " <<read_count;
		if(read_count==0)
		{
			//KALDI_LOG << "thread read count" <<read_count;
			sleep(10);
			continue;
		}else if(read_count==-1)
        {
            KALDI_LOG << "thread read count" <<read_count; 
			close(self->con_id_);
            self->running_ = false;
            break;
        }
		SocketHeader header;
		memcpy(&header, buf, header_size);
		if (header.type_ == SR_CONTENT)
		{
			std::cout << "thread " << self->con_id_ << "recv content msg length=" <<header.length_<< std::endl;
			if (header.length_)
			{
				//循环读取内容
				int pack_cnt = header.length_ / SIZE_4K;
				IntArray packages(pack_cnt, SIZE_4K);
				int last = header.length_%SIZE_4K;
				if (last > 0)
				{
					packages.push_back(last);
				}
				MsgBuffer msg_buffer = make_buffer(header.length_);
				char pack_buf[SIZE_4K];
				memset(pack_buf, 0, SIZE_4K);
				int pos = 0;
				for (int i = 0; i < packages.size();i++)
				{
					recv(self->con_id_, pack_buf, packages[i], MSG_WAITALL);
					memcpy(msg_buffer.data_ +pos, pack_buf, packages[i]);
					pos += packages[i];
				}
				self->mutex_.lock();
				self->content_list.push(msg_buffer);
				self->mutex_.unlock();
			}
		}
		else if (header.type_ == SR_QUIT)
		{   KALDI_LOG<< "退出线程header.type_ == SR_QUIT！！！";
			close(self->con_id_);
            self->running_ = false;
		}
	}
    KALDI_LOG<< "退出线程socket_thread_proc！！！";
}

void SRSocketClient::sr_thread_proc(void* self_ptr)
{   
    KALDI_LOG<< "开始线程sr_thread_proc！！！";
	SRSocketClient* self = (SRSocketClient*)self_ptr;
	while (self->running_)
	{
		if (self->content_list.size())
		{
            KALDI_LOG<< "thread " << self->con_id_ << "start proc wav";

			self->mutex_.lock();
			MsgBuffer buffer = self->content_list.front();
			self->content_list.pop();
			self->mutex_.unlock();
			//////////语音识别
			//
            KALDI_LOG<< "开始识别！！！";
			self->wav_proc(buffer);
            KALDI_LOG<< "识别结束！！！";
		}
        else{
        sleep(5);
        }
	}
    KALDI_LOG<< "退出线程sr_thread_proc！！！";
}

int replaceString(string &s1, const string &s2, const string &s3)
{
    string::size_type pos = 0;
	string::size_type a = s2.size();
	string::size_type b = s3.size();
	while ((pos = s1.find(s2, pos)) != string::npos)
	{
		s1.replace(pos, a, s3);
		pos += b;
	}
	return 0;
}

void SpeechRecg(int cnt,std::string &result)
{	
    
    //KALDI_LOG << "初始化[1]:一些参数";
    RandomAccessBaseFloatMatrixReader online_ivector_reader(
        SRHelper::instance()->online_ivector_rspecifier);
    RandomAccessBaseFloatVectorReaderMapped ivector_reader(
        SRHelper::instance()->ivector_rspecifier, SRHelper::instance()->utt2spk_rspecifier);

    Int32VectorWriter words_writer(SRHelper::instance()->words_wspecifier);
    Int32VectorWriter alignment_writer(SRHelper::instance()->alignment_wspecifier);
        
    double tot_like = 0.0;
    kaldi::int64 frame_count = 0;
    int num_success = 0, num_fail = 0;
    CachingOptimizingCompiler compiler(SRHelper::instance()->am_nnet.GetNnet(), SRHelper::instance()->decodable_opts.optimize_config);
    //KALDI_LOG << "初始化[1]:初始化完成";
    //KALDI_LOG << "提取语音特征[1]:开始提取";
    DealRecvSpeech(std::to_string(cnt));
    //KALDI_LOG << "提取语音特征[1]:特区完成";
    //KALDI_LOG << "********************后续修改项**************************";
    //KALDI_LOG << "读取feature_rspecifier"<<SRHelper::instance()->feature_rspecifier;
    //KALDI_LOG << "读取fst_in_str"<<SRHelper::instance()->fst_in_str;
    //KALDI_LOG << "*************************************************************";
    //const string &s2="test";
    replaceString(SRHelper::instance()->feature_rspecifier,s2,std::to_string(cnt));
	s2=std::to_string(cnt);

    //KALDI_LOG << "********************修改后的结果**************************";
    //KALDI_LOG << "读取feature_rspecifier"<<SRHelper::instance()->feature_rspecifier;
    if (ClassifyRspecifier(SRHelper::instance()->fst_in_str, NULL, NULL) == kNoRspecifier) {
        //KALDI_LOG << "ClassifyRspecifier==kNoRspecifier"<<kNoRspecifier<<"IS OK!!!!!!!!!!!";
        SequentialBaseFloatMatrixReader feature_reader(SRHelper::instance()->feature_rspecifier);
        KALDI_LOG <<"feature_reader IS OK!!!!";
        // Input FST is just one FST, not a table of FSTs.
        //Fst<StdArc> *decode_fst = fst::ReadFstKaldiGeneric(SRHelper::instance()->fst_in_str);
        Fst<StdArc>* decode_fst = SRHelper::instance()->decode_fst;
        SRHelper::instance()->timer.Reset();
        //KALDI_LOG << "重启计时器";

        {
            LatticeFasterDecoder decoder(*decode_fst, SRHelper::instance()->config);
            //KALDI_LOG << "循环识别";
            for (; !feature_reader.Done(); feature_reader.Next()) {
                std::string utt = feature_reader.Key();
                const Matrix<BaseFloat>& features(feature_reader.Value());
                if (features.NumRows() == 0) {
                    KALDI_WARN << "Zero-length utterance: " << utt;
                    num_fail++;
                    continue;
                }
                
                const Matrix<BaseFloat>* online_ivectors = NULL;
                const Vector<BaseFloat>* ivector = NULL;
                if (!SRHelper::instance()->ivector_rspecifier.empty()) {
                    if (!ivector_reader.HasKey(utt)) {
                        KALDI_WARN << "No iVector available for utterance " << utt;
                        num_fail++;
                        continue;
                    } else {

                        ivector = &ivector_reader.Value(utt);
                    }
                }
                //KALDI_LOG << "查找问题[1]";
                if (!SRHelper::instance()->online_ivector_rspecifier.empty()) {
                    if (!online_ivector_reader.HasKey(utt)) {
                        KALDI_WARN << "No online iVector available for utterance " << utt;
                        num_fail++;
                        continue;
                    } else {
                        online_ivectors = &online_ivector_reader.Value(utt);
                    }
                }
                //KALDI_LOG << "查找问题[2]";
                DecodableAmNnetSimple nnet_decodable(
                    SRHelper::instance()->decodable_opts, SRHelper::instance()->trans_model, SRHelper::instance()->am_nnet,
                    features, ivector, online_ivectors,
                    SRHelper::instance()->online_ivector_period, &compiler);
                //KALDI_LOG << "查找问题[3]";
                double like;
                if (DecodeUtteranceLatticeFaster(
                            decoder, nnet_decodable, SRHelper::instance()->trans_model, SRHelper::instance()->word_syms, utt,
                            SRHelper::instance()->decodable_opts.acoustic_scale, SRHelper::instance()->determinize, SRHelper::instance()->allow_partial,
                            &alignment_writer, &words_writer, &SRHelper::instance()->compact_lattice_writer,
                            &SRHelper::instance()->lattice_writer,
                            &like, result)) {
                    tot_like += like;
                    frame_count += nnet_decodable.NumFramesReady();
                    num_success++;
                } else num_fail++;
                //KALDI_LOG << "结束";
            }
        }
    }
    KALDI_LOG << "识别结果:" << result;
}

void DealRecvSpeech(string filename)
{   
	//KALDI_LOG << "filename:" << filename;
    string filepath ="data/fbank/"+filename+"/";
    //KALDI_LOG << "创建:" << filepath;
    char mkcom[128];
	sprintf(mkcom,"mkdir -p %s",filepath.c_str());
    system(mkcom);
    //KALDI_LOG << "创建"<< filepath<<"成功";
    //KALDI_LOG << "生成配置文件";
    string filenamel="CMCROSS_"+filename;
    string txtpath =filepath+"text";
    string wavpath =filepath+"wav.scp";
    string utt2spkpath = filepath+"utt2spk";
    string spk2uttpath = filepath+"spk2utt";    // delete_path(filepath);
    //KALDI_LOG << "txtpath:" << txtpath;
    //KALDI_LOG << "wavpath:" << wavpath;
    //KALDI_LOG << "utt2spkpath:" << utt2spkpath;
    //KALDI_LOG << "spk2uttpath:" << spk2uttpath;
    try{
        std::ofstream txt;
        txt.open(txtpath,std::ios::ate);
        //string txtcontent=filenamel+""+"1"+"\n";
        char txtcontent[50];
        sprintf(txtcontent,"%s 1",filenamel.c_str());
        txt<<txtcontent<<std::endl;
        txt.close();

        std::ofstream wav;
        wav.open(wavpath,std::ios::trunc);
        //string wavcontent=filenamel+" "+"data/wav/00030/"+filename+".wav"+"\n";
        char wavcontent[50];
        sprintf(wavcontent,"%s data/wav/00030/%s.wav",filenamel.c_str(),filename.c_str());
        //KALDI_LOG << "99999999999999999999999:" << wavcontent;
        wav<<wavcontent<<std::endl;
        wav.close();

        std::ofstream utt2spk;
        utt2spk.open(utt2spkpath,std::ios::trunc);
        //string utt2content=filenamel+" "+filenamel+"\n";
        char utt2content[50];
        sprintf(utt2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        utt2spk<<utt2content<<std::endl;
        utt2spk.close();

        std::ofstream spk2utt;
        spk2utt.open(spk2uttpath,std::ios::trunc);
        //string spk2content=filenamel+" "+filenamel+"\n";
        char spk2content[50];
        sprintf(spk2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        spk2utt<<spk2content<<std::endl;
        spk2utt.close();
        //KALDI_LOG << "生成配置文件成功";
		char conmand[128];
        //KALDI_LOG << "根据配置文件生成特征文件";
		sprintf(conmand,"sh demo.sh %s",filename.c_str());
        //KALDI_LOG << "command:" << conmand;
		system(conmand);
		//KALDI_LOG << "ssssssss:" << aa<<"bbbbbbbbbbb:"<<bb;
        KALDI_LOG << "生成特征文件成功";
    }catch(const std::exception &e){
        std::cerr << e.what();
    }
    
}

void SRSocketClient::wav_proc(MsgBuffer buffer)
{
	char wav_info_header[16];
	memcpy(wav_info_header, buffer.data_, 16);
	//char* result =sr_proc()
	std::string path = "./data/wav/00030/" + std::to_string(con_id_) + ".wav";

	std::ofstream fout(path);
	fout.write(buffer.data_ + 16, buffer.length_ - 16);
    KALDI_LOG << "save wav file:"<<path;
	fout.close();
    


	std::string result;
    //result="TEST";
    KALDI_LOG << "start recg!!!con_id is："<<con_id_;
	SpeechRecg(con_id_,result);

    if (SRHelper::instance()->determinize) {
            SRHelper::instance()->compact_lattice_writer.Flush();
            //SRHelper::instance()->compact_lattice_writer.Close();
        } else {
            SRHelper::instance()->lattice_writer.Flush();
            //SRHelper::instance()->lattice_writer.Close();
        }
    //KALDI_LOG << "result:"<<result;
	//.do some proc;
	char* retrun_buffer = new char[result.length() + 16];

	memcpy(retrun_buffer, wav_info_header, 16);
	memcpy(retrun_buffer + 16, result.c_str(), result.length());

	send(con_id_, retrun_buffer, result.length() + 16,0);
	delete buffer.data_;
}

bool SRSocketClient::is_ok()
{
	return running_;
}


/**********************************************************************************/

SRSocketSever::SRSocketSever(int cnt,int port)
	:client_count_(cnt),port_(port)
{
	
}

SRSocketSever::~SRSocketSever()
{

}

void SRSocketSever::start()
{
	int  slisten, sClient;
    struct sockaddr_in  servaddr;


    if( (slisten = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
        return ;
    }
    KALDI_LOG<<"----init socket----\n";

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port_);

	//绑定IP和端口  
	if( bind(slisten, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);
        return ;
    }
    KALDI_LOG<<"----bind sucess----\n";
	//开始监听  
	if (listen(slisten, 5) == -1)
	{
		KALDI_LOG<<"listen error !";
		return ;
	}

	//循环接收数据  
	//SOCKET sClient;
	sockaddr_in remoteAddr;
	socklen_t nAddrlen = sizeof(remoteAddr);
	while (true)
	{
		KALDI_LOG<<"等待连接...\n";
		sClient = accept(slisten, (struct sockaddr*)&remoteAddr, &nAddrlen);
		if (sClient == -1)
		{
			KALDI_LOG<<"accept error !";
			continue;
		}
		for (int i = 0; i < clients_.size(); i++)
		{
			if (!clients_[i]->is_ok())
			{
				delete clients_[i];
				clients_.erase(clients_.begin() + i);
			}
		}
		if (clients_.size() < client_count_)
		{
		
			KALDI_LOG<<"接收到一个连接："<<sClient;
			SRSocketClient* client = new SRSocketClient(sClient);
			clients_.push_back(client);
			client->run();
		}
		else
		{
			KALDI_LOG<<"连接达到上限拒绝连接："<<sClient;
			close(sClient);
		}
	}
}
