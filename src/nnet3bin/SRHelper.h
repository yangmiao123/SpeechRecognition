#pragma once
#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "tree/context-dep.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "decoder/decoder-wrappers.h"
#include "nnet3/nnet-am-decodable-simple.h"
#include "nnet3/nnet-utils.h"
#include "base/timer.h"
#include <string.h>
#include <iostream>
using namespace kaldi;
using namespace kaldi::nnet3;
typedef kaldi::int32 int32;
using fst::SymbolTable;
using fst::Fst;
using fst::StdArc;
class SRHelper {
private:
	SRHelper() {
        
    }
	~SRHelper() {}
public:
	static SRHelper* instance()
	{
		static SRHelper helper;
		return &helper;
	}

public:
	int a;
    int b;
    int c;
	bool allow_partial = false;
	Timer timer;
 
    LatticeFasterDecoderConfig config;
    NnetSimpleComputationOptions decodable_opts;
    
	std::string word_syms_filename;
    std::string ivector_rspecifier,
        online_ivector_rspecifier,
        utt2spk_rspecifier;
        
    int32 online_ivector_period = 0;
	std::string model_in_filename,
                fst_in_str,
                feature_rspecifier,
                lattice_wspecifier,
                words_wspecifier,
                alignment_wspecifier;

    bool determinize;
	TransitionModel trans_model;
    AmNnetSimple am_nnet;
    
    CompactLatticeWriter compact_lattice_writer;
    LatticeWriter lattice_writer;
    
    fst::SymbolTable* word_syms = NULL;
    Fst<StdArc> *decode_fst;
};
