#include "SRSocket.h"
#include "SRHelper.h"

void kaldiInit(int argc, char* argv[])
{
    KALDI_LOG << "TEST SRHelper!";
    //SRHelper::instance()->a = 1;
    KALDI_LOG << "SRHelper IS OK!";
    using namespace kaldi;
    using namespace kaldi::nnet3;
    typedef kaldi::int32 int32;
    using fst::SymbolTable;
    using fst::Fst;
    using fst::StdArc;
    KALDI_LOG << "fst IS OK!";
    
    const char* usage =
        "Generate lattices using nnet3 neural net model.\n"
        "Usage: nnet3-latgen-faster [options] <nnet-in> <fst-in|fsts-rspecifier> <features-rspecifier>"
        " <lattice-wspecifier> [ <words-wspecifier> [<alignments-wspecifier>] ]\n"
        "See also: nnet3-latgen-faster-parallel, nnet3-latgen-faster-batch\n";
    ParseOptions po(usage);
    KALDI_LOG << "po INIT IS OK!!!!!!!!!!!!!!!";
    
    SRHelper::instance()->config.Register(&po);
    SRHelper::instance()->decodable_opts.Register(&po);
    KALDI_LOG << "SRHelper::instance()->config IS OK!!!!!!!!!!!!!!!";
    po.Register("word-symbol-table", &SRHelper::instance()->word_syms_filename,
                "Symbol table for words [for debug output]");
    po.Register("allow-partial", &SRHelper::instance()->allow_partial,
                "If true, produce output even if end state was not reached.");
    po.Register("ivectors", &SRHelper::instance()->ivector_rspecifier, "Rspecifier for "
                "iVectors as vectors (i.e. not estimated online); per utterance "
                "by default, or per speaker if you provide the --utt2spk option.");
    po.Register("utt2spk", &SRHelper::instance()->utt2spk_rspecifier, "Rspecifier for "
                "utt2spk option used to get ivectors per speaker");
    po.Register("online-ivectors", &SRHelper::instance()->online_ivector_rspecifier, "Rspecifier for "
                "iVectors estimated online, as matrices.  If you supply this,"
                " you must set the --online-ivector-period option.");
    po.Register("online-ivector-period", &SRHelper::instance()->online_ivector_period, "Number of frames "
                "between iVectors in matrices supplied to the --online-ivectors "
                "option");
    po.Read(argc, argv);
	
    KALDI_LOG << "PO OPTION IS OK!!!!!!!!!!!!!!!";
    if (po.NumArgs() < 4 || po.NumArgs() > 6) {
        po.PrintUsage();
        exit(1);
    }
    
    SRHelper::instance()->model_in_filename = po.GetArg(1);
    SRHelper::instance()->fst_in_str = po.GetArg(2);
    SRHelper::instance()->feature_rspecifier = po.GetArg(3);
    SRHelper::instance()->lattice_wspecifier = po.GetArg(4);
    SRHelper::instance()->words_wspecifier = po.GetOptArg(5);
    SRHelper::instance()->alignment_wspecifier = po.GetOptArg(6);

    SRHelper::instance()->determinize = SRHelper::instance()->config.determinize_lattice;
    SRHelper::instance()->am_nnet;
    {
        bool binary;
        Input ki(SRHelper::instance()->model_in_filename, &binary);
        SRHelper::instance()->trans_model.Read(ki.Stream(), binary);
        SRHelper::instance()->am_nnet.Read(ki.Stream(), binary);
        SetBatchnormTestMode(true, &(SRHelper::instance()->am_nnet.GetNnet()));
        SetDropoutTestMode(true, &(SRHelper::instance()->am_nnet.GetNnet()));
        CollapseModel(CollapseModelConfig(), &(SRHelper::instance()->am_nnet.GetNnet()));
    }
    SRHelper::instance()->determinize = SRHelper::instance()->config.determinize_lattice;
    
    if (! (SRHelper::instance()->determinize ? SRHelper::instance()->compact_lattice_writer.Open(SRHelper::instance()->lattice_wspecifier)
           : SRHelper::instance()->lattice_writer.Open(SRHelper::instance()->lattice_wspecifier)))
      KALDI_ERR << "Could not open table for writing lattices: "
                 << SRHelper::instance()->lattice_wspecifier;

                 
    if (SRHelper::instance()->word_syms_filename != "")
        if (!(SRHelper::instance()->word_syms = fst::SymbolTable::ReadText(SRHelper::instance()->word_syms_filename)))
            KALDI_ERR << "Could not read symbol table from file "
                      << SRHelper::instance()->word_syms_filename;
    SRHelper::instance()->decode_fst = fst::ReadFstKaldiGeneric(SRHelper::instance()->fst_in_str);
    
}
int main(int argc, char* argv[])
{   
    KALDI_LOG << "START!";
    kaldiInit(argc,argv);

    KALDI_LOG << "kaldiInit IS OK!";
	SRSocketSever server(5,8989);
    KALDI_LOG << "server INIT IS OK!";
	server.start();
    KALDI_LOG << "server STOP!";
	return 0;
}