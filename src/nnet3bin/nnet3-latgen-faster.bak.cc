#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <dirent.h>
#include <sys/time.h>
#include <fstream>


#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "tree/context-dep.h"
#include "hmm/transition-model.h"
#include "fstext/fstext-lib.h"
#include "decoder/decoder-wrappers.h"
#include "nnet3/nnet-am-decodable-simple.h"
#include "nnet3/nnet-utils.h"
#include "base/timer.h"
using namespace std;
#define MAXLINE 4096

void delete_path(const char* path){
	DIR *pDir = NULL;
	struct dirent *dmsg;
	char szFileName[128];
	char szFolderName[128];
 
	strcpy(szFolderName, path);
	strcat(szFolderName, "/%s");
	if ((pDir = opendir(path)) != NULL)
	{
		// 遍历目录并删除文件
		while ((dmsg = readdir(pDir)) != NULL)
		{
			if (strcmp(dmsg->d_name, ".") != 0 && strcmp(dmsg->d_name, "..") != 0)
			{
				sprintf(szFileName, szFolderName, dmsg->d_name);
				string tmp = szFileName;
				//如果是文件夹，名称中不包含"."
				if (tmp.find(".") == -1){
					delete_path(szFileName);
				}
				remove(szFileName);
			}
		}
	}

	if (pDir != NULL)
	{
		closedir(pDir);
	}
}

void DealRecvSpeech(string filename)
{   
    KALDI_LOG << "filename:" << filename;
    string filepath ="data/fbank/test/";
    string filenamel="CMCROSS_"+filename;
    string txtpath =filepath+"text";
    string wavpath =filepath+"wav.scp";
    string utt2spkpath = filepath+"utt2spk";
    string spk2uttpath = filepath+"spk2utt";    // delete_path(filepath);
    KALDI_LOG << "txtpath:" << txtpath;
    KALDI_LOG << "wavpath:" << wavpath;
    KALDI_LOG << "utt2spkpath:" << utt2spkpath;
    KALDI_LOG << "spk2uttpath:" << spk2uttpath;
    try{
        ofstream txt;
        txt.open(txtpath,ios::ate);
        //string txtcontent=filenamel+""+"1"+"\n";
        char txtcontent[50];
        sprintf(txtcontent,"%s 1",filenamel.c_str());
        txt<<txtcontent<<endl;
        txt.close();

        ofstream wav;
        wav.open(wavpath,ios::trunc);
        //string wavcontent=filenamel+" "+"data/wav/00030/"+filename+".wav"+"\n";
        char wavcontent[50];
        sprintf(wavcontent,"%s data/wav/00030/%s.wav",filenamel.c_str(),filename.c_str());
        KALDI_LOG << "99999999999999999999999:" << wavcontent;
        wav<<wavcontent<<endl;
        wav.close();

        ofstream utt2spk;
        utt2spk.open(utt2spkpath,ios::trunc);
        //string utt2content=filenamel+" "+filenamel+"\n";
        char utt2content[50];
        sprintf(utt2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        utt2spk<<utt2content<<endl;
        utt2spk.close();

        ofstream spk2utt;
        spk2utt.open(spk2uttpath,ios::trunc);
        //string spk2content=filenamel+" "+filenamel+"\n";
        char spk2content[50];
        sprintf(spk2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        spk2utt<<spk2content<<endl;
        spk2utt.close();

        int aa=system("sh dealspeech.sh");
        int bb=system("pwd");
        KALDI_LOG << "ssssssss:" << aa<<"bbbbbbbbbbb:"<<bb;
    }catch(const std::exception &e){
        std::cerr << e.what();
    }
    
}

int main(int argc, char* argv[]) {
    struct timeval t1, t2, t3, t4;
    gettimeofday(&t1, NULL);
    int  listenfd, connfd;
    struct sockaddr_in  servaddr;
    char  buff[MAXLINE];
    FILE *fp;
    int  n;

    if( (listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }
    printf("----init socket----\n");

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(8090);
    //���ö˿ڿ�����
    int contain;
    setsockopt(listenfd,SOL_SOCKET, SO_REUSEADDR, &contain, sizeof(int));

    if( bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }
    printf("----bind sucess----\n");

    if( listen(listenfd, 10) == -1){
        printf("listen socket error: %s(errno: %d)\n",strerror(errno),errno);
        return 0;
    }
    printf("======waiting for client's request======\n");
    
    using namespace kaldi;
    using namespace kaldi::nnet3;
    typedef kaldi::int32 int32;
    using fst::SymbolTable;
    using fst::Fst;
    using fst::StdArc;

    const char* usage =
        "Generate lattices using nnet3 neural net model.\n"
        "Usage: nnet3-latgen-faster [options] <nnet-in> <fst-in|fsts-rspecifier> <features-rspecifier>"
        " <lattice-wspecifier> [ <words-wspecifier> [<alignments-wspecifier>] ]\n"
        "See also: nnet3-latgen-faster-parallel, nnet3-latgen-faster-batch\n";
    ParseOptions po(usage);
    Timer timer;
    bool allow_partial = false;
    LatticeFasterDecoderConfig config;
    NnetSimpleComputationOptions decodable_opts;

    std::string word_syms_filename;
    std::string ivector_rspecifier,
        online_ivector_rspecifier,
        utt2spk_rspecifier;
    int32 online_ivector_period = 0;
    config.Register(&po);
    decodable_opts.Register(&po);
    po.Register("word-symbol-table", &word_syms_filename,
                "Symbol table for words [for debug output]");
    po.Register("allow-partial", &allow_partial,
                "If true, produce output even if end state was not reached.");
    po.Register("ivectors", &ivector_rspecifier, "Rspecifier for "
                "iVectors as vectors (i.e. not estimated online); per utterance "
                "by default, or per speaker if you provide the --utt2spk option.");
    po.Register("utt2spk", &utt2spk_rspecifier, "Rspecifier for "
                "utt2spk option used to get ivectors per speaker");
    po.Register("online-ivectors", &online_ivector_rspecifier, "Rspecifier for "
                "iVectors estimated online, as matrices.  If you supply this,"
                " you must set the --online-ivector-period option.");
    po.Register("online-ivector-period", &online_ivector_period, "Number of frames "
                "between iVectors in matrices supplied to the --online-ivectors "
                "option");

    po.Read(argc, argv);

    if (po.NumArgs() < 4 || po.NumArgs() > 6) {
        po.PrintUsage();
        exit(1);
    }

    std::string model_in_filename = po.GetArg(1),
                fst_in_str = po.GetArg(2),
                feature_rspecifier = po.GetArg(3),
                lattice_wspecifier = po.GetArg(4),
                words_wspecifier = po.GetOptArg(5),
                alignment_wspecifier = po.GetOptArg(6);
    KALDI_LOG << "model_in_filename:" << model_in_filename;
    KALDI_LOG << "fst_in_str:" << fst_in_str;
    KALDI_LOG << "feature_rspecifier:" << feature_rspecifier;
    KALDI_LOG << "lattice_wspecifier:" << lattice_wspecifier;

    TransitionModel trans_model;
    AmNnetSimple am_nnet;
    {
        bool binary;
        Input ki(model_in_filename, &binary);
        trans_model.Read(ki.Stream(), binary);
        am_nnet.Read(ki.Stream(), binary);
        SetBatchnormTestMode(true, &(am_nnet.GetNnet()));
        SetDropoutTestMode(true, &(am_nnet.GetNnet()));
        CollapseModel(CollapseModelConfig(), &(am_nnet.GetNnet()));
    }
    gettimeofday(&t2, NULL);
    double deltaT = ((t2.tv_usec - t1.tv_usec) + (t2.tv_sec - t1.tv_sec) * 1000000) / 1000;

    fst::SymbolTable* word_syms = NULL;
    if (word_syms_filename != "")
        if (!(word_syms = fst::SymbolTable::ReadText(word_syms_filename)))
            KALDI_ERR << "Could not read symbol table from file "
                      << word_syms_filename;
    Fst<StdArc>* decode_fst = fst::ReadFstKaldiGeneric(fst_in_str);
    LatticeFasterDecoder decoder(*decode_fst, config);

    bool determinize = config.determinize_lattice;
    CompactLatticeWriter compact_lattice_writer;
    LatticeWriter lattice_writer;

    RandomAccessBaseFloatMatrixReader online_ivector_reader(online_ivector_rspecifier);
    RandomAccessBaseFloatVectorReaderMapped ivector_reader(ivector_rspecifier, utt2spk_rspecifier);

    KALDI_LOG << "ivector_rspecifier:" << ivector_rspecifier;
    KALDI_LOG << "utt2spk_rspecifier:" << utt2spk_rspecifier;

    Int32VectorWriter words_writer(words_wspecifier);
    Int32VectorWriter alignment_writer(alignment_wspecifier);



    std::string result;
    double tot_like = 0.0;
    kaldi::int64 frame_count = 0;
    int num_success = 0, num_fail = 0;
    // this compiler object allows caching of computations across
    // different utterances.
    CachingOptimizingCompiler compiler(am_nnet.GetNnet(), decodable_opts.optimize_config);



    KALDI_LOG << "server start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n" << "use time:" << deltaT << "ms";
    
    while(1){
        gettimeofday(&t3, NULL);
        struct sockaddr_in client_addr;
        socklen_t size=sizeof(client_addr);
        if( (connfd = accept(listenfd, (struct sockaddr*)&client_addr, &size)) == -1){
            printf("accept socket error: %s(errno: %d)",strerror(errno),errno);
            continue;
            //sleep();
        }
        if((fp = fopen("./data/wav/00030/0.wav","wb") ) == NULL )
        {
        printf("File.\n");
        close(listenfd);
        exit(1);
        }
        while(1){
            n = read(connfd, buff, MAXLINE);
            //std::cout<<"recive:"<< n << endl;
            if(n < MAXLINE)
            {
                //std::cout<<"file recive end: \n"<<n;
                break;
            }
            fwrite(buff, 1, n, fp); 
        }

        bzero(buff, MAXLINE);
        fclose(fp);
        DealRecvSpeech("0");
        if (!(determinize ? compact_lattice_writer.Open(lattice_wspecifier) : lattice_writer.Open(lattice_wspecifier)))
            KALDI_ERR << "Could not open table for writing lattices: " << lattice_wspecifier;

        if (ClassifyRspecifier(fst_in_str, NULL, NULL) == kNoRspecifier) {
            KALDI_LOG << "feature_rspecifier:" << feature_rspecifier;
            SequentialBaseFloatMatrixReader feature_reader(feature_rspecifier);
            // Input FST is just one FST, not a table of FSTs.

            timer.Reset();

            {

                for (; !feature_reader.Done(); feature_reader.Next()) {
                    std::string utt = feature_reader.Key();
                    const Matrix<BaseFloat>& features(feature_reader.Value());
                    if (features.NumRows() == 0) {
                        KALDI_WARN << "Zero-length utterance: " << utt;
                        num_fail++;
                        continue;
                    }
                    KALDI_LOG << "1111111111111111111111111111111111";
                    KALDI_LOG << "ivector_rspecifier:" << ivector_rspecifier;
                    const Matrix<BaseFloat>* online_ivectors = NULL;
                    const Vector<BaseFloat>* ivector = NULL;
                    if (!ivector_rspecifier.empty()) {
                        if (!ivector_reader.HasKey(utt)) {
                            KALDI_WARN << "No iVector available for utterance " << utt;
                            num_fail++;
                            continue;
                        } else {

                            ivector = &ivector_reader.Value(utt);
                        }
                    }
                    KALDI_LOG << "22222222222222222222222222222222222222";
                    KALDI_LOG << "online_ivector_rspecifier:" << online_ivector_rspecifier;
                    KALDI_LOG << "utt:" << utt;
                    if (!online_ivector_rspecifier.empty()) {
                        if (!online_ivector_reader.HasKey(utt)) {
                            KALDI_WARN << "No online iVector available for utterance " << utt;
                            num_fail++;
                            continue;
                        } else {
                            online_ivectors = &online_ivector_reader.Value(utt);
                        }
                    }

                    DecodableAmNnetSimple nnet_decodable(
                        decodable_opts, trans_model, am_nnet,
                        features, ivector, online_ivectors,
                        online_ivector_period, &compiler);

                    double like;
                    if (DecodeUtteranceLatticeFaster(
                                decoder, nnet_decodable, trans_model, word_syms, utt,
                                decodable_opts.acoustic_scale, determinize, allow_partial,
                                &alignment_writer, &words_writer, &compact_lattice_writer,
                                &lattice_writer,
                                &like, result)) {
                        tot_like += like;
                        frame_count += nnet_decodable.NumFramesReady();
                        num_success++;
                    } else num_fail++;
                }
            }
            
            
            gettimeofday(&t4, NULL);
            double deltaT = t4.tv_sec - t3.tv_sec;
            KALDI_LOG << "rec end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n" << "use time:" << deltaT << "s";
        //feature_reader.Close();  
        }

        send(connfd,result.c_str(),result.length(),0);     
        close(connfd);
        close(connfd);
        KALDI_LOG << result;
        result.clear();
        
        if (determinize) {
            compact_lattice_writer.Flush();
            compact_lattice_writer.Close();
        } else {
            lattice_writer.Flush();
            lattice_writer.Close();
        }
        KALDI_LOG << "rec end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n" << "use time:" << deltaT << "s";
    }
    //}

    close(listenfd);
    return 0;
}
