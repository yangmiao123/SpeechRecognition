﻿#include "SRSocket.h"
//#include <WS2tcpip.h>
#include <iostream>
#include "SRHelper.h"
#include <fstream>
#include <string.h>
//#include<winsock2.h>


typedef std::vector<int> IntArray;

MsgBuffer make_buffer(int lenth)
{
	MsgBuffer buffer;
	buffer.data_ = new char[lenth];
	memset(buffer.data_,0, lenth);
	buffer.length_ = lenth;
	return buffer;
}


SRSocketClient::SRSocketClient(int connect_id)
	:con_id_(connect_id)
{

}

SRSocketClient::~SRSocketClient()
{
	if (thread_.joinable())
	{
		thread_.join();
	}
	if (sr_thread_.joinable())
	{
		sr_thread_.join();
	}
}

void SRSocketClient::run()
{
	running_ = true;
	thread_ = std::thread(socket_thread_proc, this);
	sr_thread_ = std::thread(sr_thread_proc, this);
}

void SRSocketClient::socket_thread_proc(void* self_ptr)
{
	SRSocketClient* self = (SRSocketClient*)self_ptr;
	while (self->running_)
	{
        KALDI_LOG << "thread start read message!"; 
		char buf[header_size];
		int read_count = recv(self->con_id_, buf, header_size, MSG_WAITALL);
		if(read_count==0)
		{
			KALDI_LOG << "thread read count" <<read_count; 
			continue;
		}else if(read_count==-1)
        {
            KALDI_LOG << "thread read count" <<read_count; 
			close(self->con_id_);
            self->running_ = false;
            break;
        }
		SocketHeader header;
		memcpy(&header, buf, header_size);
		if (header.type_ == SR_CONTENT)
		{
			std::cout << "thread " << self->con_id_ << "recv content msg length=" <<header.length_<< std::endl;
			if (header.length_)
			{
				//循环读取内容
				int pack_cnt = header.length_ / SIZE_4K;
				IntArray packages(pack_cnt, SIZE_4K);
				int last = header.length_%SIZE_4K;
				if (last > 0)
				{
					packages.push_back(last);
				}
				MsgBuffer msg_buffer = make_buffer(header.length_);
				char pack_buf[SIZE_4K];
				memset(pack_buf, 0, SIZE_4K);
				int pos = 0;
				for (int i = 0; i < packages.size();i++)
				{
					recv(self->con_id_, pack_buf, packages[i], MSG_WAITALL);
					memcpy(msg_buffer.data_ +pos, pack_buf, packages[i]);
					pos += packages[i];
				}
				self->mutex_.lock();
				self->content_list.push(msg_buffer);
				self->mutex_.unlock();
			}
		}
		else if (header.type_ == SR_QUIT)
		{
			close(self->con_id_);
            self->running_ = false;
		}
	}
}

void SRSocketClient::sr_thread_proc(void* self_ptr)
{
	SRSocketClient* self = (SRSocketClient*)self_ptr;
	while (self->running_)
	{
		if (self->content_list.size())
		{
            KALDI_LOG<< "thread " << self->con_id_ << "start proc wav";

			self->mutex_.lock();
			MsgBuffer buffer = self->content_list.front();
			self->content_list.pop();
			self->mutex_.unlock();
			//////////语音识别
			//
			self->wav_proc(buffer);
		}
	}
}

void SpeechRecg(int cnt,std::string &result)
{	
    double tot_like = 0.0;
    kaldi::int64 frame_count = 0;
    int num_success = 0, num_fail = 0;
    CachingOptimizingCompiler compiler(SRHelper::instance()->am_nnet.GetNnet(), SRHelper::instance()->decodable_opts.optimize_config);
    RandomAccessBaseFloatMatrixReader online_ivector_reader(SRHelper::instance()->online_ivector_rspecifier);
    RandomAccessBaseFloatVectorReaderMapped ivector_reader(SRHelper::instance()->ivector_rspecifier, SRHelper::instance()->utt2spk_rspecifier);
    fst::SymbolTable* word_syms = NULL;
    if (SRHelper::instance()->word_syms_filename != "")
        if (!(word_syms = fst::SymbolTable::ReadText(SRHelper::instance()->word_syms_filename)))
            KALDI_ERR << "Could not read symbol table from file "
                      << SRHelper::instance()->word_syms_filename;
    Fst<StdArc>* decode_fst = fst::ReadFstKaldiGeneric(SRHelper::instance()->fst_in_str);
    LatticeFasterDecoder decoder(*decode_fst, SRHelper::instance()->config);
    Int32VectorWriter words_writer(SRHelper::instance()->words_wspecifier);
    Int32VectorWriter alignment_writer(SRHelper::instance()->alignment_wspecifier);

	DealRecvSpeech(std::to_string(cnt));

    if (!(SRHelper::instance()->determinize ? SRHelper::instance()->compact_lattice_writer.Open(SRHelper::instance()->lattice_wspecifier) : SRHelper::instance()->lattice_writer.Open(SRHelper::instance()->lattice_wspecifier)))
            KALDI_ERR << "Could not open table for writing lattices: " << SRHelper::instance()->lattice_wspecifier;

        if (ClassifyRspecifier(SRHelper::instance()->fst_in_str, NULL, NULL) == kNoRspecifier) {
      
            SequentialBaseFloatMatrixReader feature_reader(SRHelper::instance()->feature_rspecifier);
            // Input FST is just one FST, not a table of FSTs.

            SRHelper::instance()->timer.Reset();

            {

                for (; !feature_reader.Done(); feature_reader.Next()) {
                    std::string utt = feature_reader.Key();
                    const Matrix<BaseFloat>& features(feature_reader.Value());
                    if (features.NumRows() == 0) {
                        KALDI_WARN << "Zero-length utterance: " << utt;
                        num_fail++;
                        continue;
                    }
                    
                    const Matrix<BaseFloat>* online_ivectors = NULL;
                    const Vector<BaseFloat>* ivector = NULL;
                    if (!SRHelper::instance()->ivector_rspecifier.empty()) {
                        if (!ivector_reader.HasKey(utt)) {
                            KALDI_WARN << "No iVector available for utterance " << utt;
                            num_fail++;
                            continue;
                        } else {

                            ivector = &ivector_reader.Value(utt);
                        }
                    }
                    
                    if (!SRHelper::instance()->online_ivector_rspecifier.empty()) {
                        if (!online_ivector_reader.HasKey(utt)) {
                            KALDI_WARN << "No online iVector available for utterance " << utt;
                            num_fail++;
                            continue;
                        } else {
                            online_ivectors = &online_ivector_reader.Value(utt);
                        }
                    }

                    DecodableAmNnetSimple nnet_decodable(
                        SRHelper::instance()->decodable_opts, SRHelper::instance()->trans_model, SRHelper::instance()->am_nnet,
                        features, ivector, online_ivectors,
                        SRHelper::instance()->online_ivector_period, &compiler);

                    double like;
                    if (DecodeUtteranceLatticeFaster(
                                decoder, nnet_decodable, SRHelper::instance()->trans_model, word_syms, utt,
                                SRHelper::instance()->decodable_opts.acoustic_scale, SRHelper::instance()->determinize, SRHelper::instance()->allow_partial,
                                &alignment_writer, &words_writer, &SRHelper::instance()->compact_lattice_writer,
                                &SRHelper::instance()->lattice_writer,
                                &like, result)) {
                        tot_like += like;
                        frame_count += nnet_decodable.NumFramesReady();
                        num_success++;
                    } else num_fail++;
                }
            }
        }
}

void DealRecvSpeech(string filename)
{   
	KALDI_LOG << "filename:" << filename;
    string filepath ="data/fbank/"+filename+"/";
    string filenamel="CMCROSS_"+filename;
    string txtpath =filepath+"text";
    string wavpath =filepath+"wav.scp";
    string utt2spkpath = filepath+"utt2spk";
    string spk2uttpath = filepath+"spk2utt";    // delete_path(filepath);
    KALDI_LOG << "txtpath:" << txtpath;
    KALDI_LOG << "wavpath:" << wavpath;
    KALDI_LOG << "utt2spkpath:" << utt2spkpath;
    KALDI_LOG << "spk2uttpath:" << spk2uttpath;
    try{
        std::ofstream txt;
        txt.open(txtpath,std::ios::ate);
        //string txtcontent=filenamel+""+"1"+"\n";
        char txtcontent[50];
        sprintf(txtcontent,"%s 1",filenamel.c_str());
        txt<<txtcontent<<std::endl;
        txt.close();

        std::ofstream wav;
        wav.open(wavpath,std::ios::trunc);
        //string wavcontent=filenamel+" "+"data/wav/00030/"+filename+".wav"+"\n";
        char wavcontent[50];
        sprintf(wavcontent,"%s data/wav/00030/%s.wav",filenamel.c_str(),filename.c_str());
        KALDI_LOG << "99999999999999999999999:" << wavcontent;
        wav<<wavcontent<<std::endl;
        wav.close();

        std::ofstream utt2spk;
        utt2spk.open(utt2spkpath,std::ios::trunc);
        //string utt2content=filenamel+" "+filenamel+"\n";
        char utt2content[50];
        sprintf(utt2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        utt2spk<<utt2content<<std::endl;
        utt2spk.close();

        std::ofstream spk2utt;
        spk2utt.open(spk2uttpath,std::ios::trunc);
        //string spk2content=filenamel+" "+filenamel+"\n";
        char spk2content[50];
        sprintf(spk2content,"%s %s",filenamel.c_str(),filenamel.c_str());
        spk2utt<<spk2content<<std::endl;
        spk2utt.close();
        
		char conmand[128];
		sprintf(conmand,"sh demo.sh %s",filename.c_str());
		int aa=system(conmand);
		int bb=system("pwd");
		KALDI_LOG << "ssssssss:" << aa<<"bbbbbbbbbbb:"<<bb;
    }catch(const std::exception &e){
        std::cerr << e.what();
    }
    
}

void SRSocketClient::wav_proc(MsgBuffer buffer)
{
	char wav_info_header[16];
	memcpy(wav_info_header, buffer.data_, 16);
	//char* result =sr_proc()
	std::string path = "./data/wav/00030/" + std::to_string(con_id_) + ".wav";

	std::ofstream fout(path);
	fout.write(buffer.data_ + 16, buffer.length_ - 16);
    KALDI_LOG << "save wav file:"<<path;
	fout.close();
    


	std::string result;
    result="TEST";
	//SpeechRecg(con_id_,result);
    
	//.do some proc;
	char* retrun_buffer = new char[result.length() + 16];

	memcpy(retrun_buffer, wav_info_header, 16);
	memcpy(retrun_buffer + 16, result.c_str(), result.length());

	send(con_id_, retrun_buffer, result.length() + 16,0);
	delete buffer.data_;
}

bool SRSocketClient::is_ok()
{
	return running_;
}


/**********************************************************************************/

SRSocketSever::SRSocketSever(int cnt,int port)
	:client_count_(cnt),port_(port)
{
	
}

SRSocketSever::~SRSocketSever()
{

}

void SRSocketSever::start()
{
	int  slisten, sClient;
    struct sockaddr_in  servaddr;


    if( (slisten = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
        printf("create socket error: %s(errno: %d)\n",strerror(errno),errno);
        return ;
    }
    KALDI_LOG<<"----init socket----\n";

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port_);

	//绑定IP和端口  
	if( bind(slisten, (struct sockaddr*)&servaddr, sizeof(servaddr)) == -1){
        printf("bind socket error: %s(errno: %d)\n",strerror(errno),errno);
        return ;
    }
    KALDI_LOG<<"----bind sucess----\n";
	//开始监听  
	if (listen(slisten, 5) == -1)
	{
		KALDI_LOG<<"listen error !";
		return ;
	}

	//循环接收数据  
	//SOCKET sClient;
	sockaddr_in remoteAddr;
	socklen_t nAddrlen = sizeof(remoteAddr);
	while (true)
	{
		KALDI_LOG<<"等待连接...\n";
		sClient = accept(slisten, (struct sockaddr*)&remoteAddr, &nAddrlen);
		if (sClient == -1)
		{
			KALDI_LOG<<"accept error !";
			continue;
		}
		for (int i = 0; i < clients_.size(); i++)
		{
			if (!clients_[i]->is_ok())
			{
				delete clients_[i];
				clients_.erase(clients_.begin() + i);
			}
		}
		if (clients_.size() < client_count_)
		{
		
			KALDI_LOG<<"接收到一个连接："<<sClient;
			SRSocketClient* client = new SRSocketClient(sClient);
			clients_.push_back(client);
			client->run();
		}
		else
		{
			KALDI_LOG<<"连接达到上限拒绝连接："<<sClient;
			close(sClient);
		}
	}
}
