# SpeechRecognition

#### 介绍
语音识别服务

#### 软件架构
kaldi+scoket

#### 安装教程

1. 编译kaldi，参考：https://blog.csdn.net/baidu_26788951/article/details/83311145
2. 将安装包解压至egs文件夹下，压缩包：链接：https://pan.baidu.com/s/1zxJp30iVdDR24qHuJAvFIA 提取码：r6xd 
#### 使用说明

1. 运行run.sh
